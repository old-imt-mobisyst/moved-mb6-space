
Plage hors DHCP : 10.89.5.10 -> 10.89.5.50

Goupe A => 10.89.5.x1 avec x in [1..4]
donc 4 IPs / groupe

Groupe 1 :
- pi3 10.89.5.11
- ned2 10.89.5.21
- pc fixe en DHCP  mais pourrait prendre 10.89.5.31
- pc portable en DHCP mais pourrait prendre 10.89.5.41

Groupe 2 :
- pi3 10.89.5.12
- ned2 10.89.5.22
- pc fixe en DHCP  mais pourrait prendre 10.89.5.32
- pc portable en DHCP mais pourrait prendre 10.89.5.42

