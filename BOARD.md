# To-Do Board of Mb6-Space project:

## Basic : 

- simple_move
- transform
- parasit: 
 * Double domain
 * Parameters
 * service: exec

## TSim : 

- Launchfile avec Multi-plexer.


## UV Larm : 

- Ajouter des corrections via des lien sur basic (simple_move, transform). 
- Sortir la navigation du simulateur ?
- Ajouter le goal pose dans le challenge 1.
- Reprendre la notion de parametre (integration code, usage launchfile)
- Services, Visualisation Web...

## PiBot : 

- Integré bouton, soulévement, obstacle négatifs...
- Mettre en place le déployement de démos (multi-robot SLAM sur random-move)
- Bouton stop.
